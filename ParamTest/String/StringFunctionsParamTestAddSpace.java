
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)
public class StringFunctionsParamTestAddSpace {

    private String str;
    private String expectedString;

    public StringFunctionsParamTestAddSpace(String str, String expectedString ){
        this.str=str;
        this.expectedString=expectedString;
    }

    @Parameterized.Parameters()

    public static Iterable<Object[]> Test(){
        return Arrays.asList(new Object[][]{
                {"Hello,my,friend", "Hello, my, friend"},
                {"I,love,DevEducation", "I, love, DevEducation"},
        });
    }

    @Test
    public void addSpace(){
        Assertions.assertEquals(expectedString, new StringFunctions().addSpace(str));
    }
}
