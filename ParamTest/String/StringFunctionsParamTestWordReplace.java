
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)
public class StringFunctionsParamTestWordReplace {

    private String[] arrays;
    private int length;
    private String[] expectedString;

    public StringFunctionsParamTestWordReplace(String[] arrays, int length, String[] expectedString ){
        this.arrays=arrays;
        this.length=length;
        this.expectedString=expectedString;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test(){
        return Arrays.asList(new Object[][]{
                {new String[]{"Hello" , "my" ,"friend"},6, new String[]{"Hello","my","fri$"}},
                {new String[]{"I" , "love" ,"DevEducation"},12, new String[]{"I", "love", "DevEducat$"}},
        });
    }
    @Test
    public void wordReplace(){
        Assertions.assertEquals(expectedString, new StringFunctions().wordReplace(arrays,length));
    }
}
