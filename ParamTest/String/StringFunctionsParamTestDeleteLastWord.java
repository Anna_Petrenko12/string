
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)
public class StringFunctionsParamTestDeleteLastWord {

    private String str;
    private String expectedString;

    public StringFunctionsParamTestDeleteLastWord(String str, String expectedString ){
        this.str=str;
        this.expectedString=expectedString;
    }

    @Parameterized.Parameters()

    public static Iterable<Object[]> Test(){
        return Arrays.asList(new Object[][]{
                {"Hello, world", "Hello"},
                {"gjjj, dhjkf", "gjjj"},
        });
    }

    @Test
    public void saveOneRepeatingCharacter(){
        Assertions.assertEquals(expectedString, new StringFunctions().deleteLastWord(str));
    }
}