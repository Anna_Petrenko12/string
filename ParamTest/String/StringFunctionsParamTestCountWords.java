
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)
public class StringFunctionsParamTestCountWords {
    private String str;
    private int expectedInt;

    public StringFunctionsParamTestCountWords(String str, int expectedInt){
        this.str=str;
        this.expectedInt=expectedInt;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test(){
        return Arrays.asList(new Object[][]{
                {"Hello my friend", 3},
                {"I love DevEducation", 3},
        });
    }
    @Test
    public void countWords(){
        Assertions.assertEquals(expectedInt, new StringFunctions().countWords(str));
    }
}
