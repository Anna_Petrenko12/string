
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)
public class StringFunctionsParamTestRevers {

    private String str;
    private String expectedString;

    public StringFunctionsParamTestRevers(String str, String expectedString) {
        this.str = str;
        this.expectedString = expectedString;
    }

    @Parameterized.Parameters()

    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {"Hello, world", "dlrow ,olleH"},
                {"I, love, DevEducation", "noitacudEveD ,evol ,I"},
        });
    }

    @Test
    public void reverse() {
        Assertions.assertEquals(expectedString, new StringFunctions().reverse(str));
    }
}