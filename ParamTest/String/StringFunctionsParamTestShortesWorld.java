import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

@RunWith(Parameterized.class)
public class StringFunctionsParamTestShortesWorld {
    private String str;
    private int expectedInt;

    public StringFunctionsParamTestShortesWorld(String str, int expectedInt){
        this.str=str;
        this.expectedInt=expectedInt;

    }
    @Parameterized.Parameters()
    public static Iterable<Object[]> Test(){
        return Arrays.asList(new Object[][]{
                {"Hello, I am a new short word",1},
        {"Hello, world",5},
        });
    }
    @Test
    public void paramTest(){
        assertEquals(expectedInt, new StringFunctions().shortesWorld(str));
    }
}
