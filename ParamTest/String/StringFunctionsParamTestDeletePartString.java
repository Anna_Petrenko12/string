
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StringFunctionsParamTestDeletePartString {
    private String str;
    private int indexOf;
    private int count;
    private String expectedString;


    public StringFunctionsParamTestDeletePartString(String str, int indexOf, int count, String expectedString){
        this.str=str;
        this.indexOf=0;
        this.count=0;
        this.expectedString=expectedString;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test(){
        return Arrays.asList(new Object[][]{
                {"Hello,world", 2, 5, "Heworld"},
                {"I love DevEducation",12,6, "I love DevEducation"},
        });
    }
    @Test
    public void deletePartOfString(){
        Assertions.assertEquals(expectedString, new StringFunctions().deletePartOfString(str,indexOf,count));
    }
}
