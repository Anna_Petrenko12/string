
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)
public class StringFunctionsParamTestSaveOneRepeatingCharacter {

    private String str;
    private String expectedString;

    public StringFunctionsParamTestSaveOneRepeatingCharacter(String str, String expectedString ){
        this.str=str;
        this.expectedString=expectedString;
    }

    @Parameterized.Parameters()

    public static Iterable<Object[]> Test(){
        return Arrays.asList(new Object[][]{
                {"Hello,world", "He,world"},
                {"gjjj, dhjkf", "g, dhjkf"},
        });
    }

    @Test
    public void saveOneRepeatingCharacter(){
        Assertions.assertEquals(expectedString, new StringFunctions().saveOneRepeatingCharacter(str));
    }
}