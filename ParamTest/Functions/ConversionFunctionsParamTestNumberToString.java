import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;


@RunWith(Parameterized.class)
public class ConversionFunctionsParamTestNumberToString {

    private int number;
    private String expectedString;


    public ConversionFunctionsParamTestNumberToString(int number, String expectedString ){

        this.number=number;
        this.expectedString=expectedString;
    }
    @Parameterized.Parameters()
    public static Iterable<Object[]> Test(){
        return Arrays.asList(new Object[][]{
                {2,"2"},
        {4,"4"},
        {7,"7"},
        });
    }
    @Test
    public void numberToString(){
        Assertions.assertEquals(expectedString, new ConversionFunctions().numberToString(number));

    }

}
