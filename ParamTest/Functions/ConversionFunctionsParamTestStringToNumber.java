import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

@RunWith(Parameterized.class)
public class ConversionFunctionsParamTestStringToNumber {
    private String number;
    private int expectedInt;


    public ConversionFunctionsParamTestStringToNumber(String number, int expectedInt ){

        this.number=number;
        this.expectedInt=expectedInt;
    }
    @Parameterized.Parameters()
    public static Iterable<Object[]> Test(){
        return Arrays.asList(new Object[][]{
                {"2",2},
                {"4",4},
                {"7",7},
        });
    }
    @Test
    public void stringToNumber(){
        Assertions.assertEquals(expectedInt, new ConversionFunctions().stringToNumber(number));

    }

}
