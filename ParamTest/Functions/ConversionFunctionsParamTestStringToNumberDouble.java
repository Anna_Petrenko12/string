import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

@RunWith(Parameterized.class)
public class ConversionFunctionsParamTestStringToNumberDouble {

    private String number;
    private Double expectedDouble;


    public ConversionFunctionsParamTestStringToNumberDouble(String number, Double expectedDouble ){

        this.number=number;
        this.expectedDouble=expectedDouble;
    }
    @Parameterized.Parameters()
    public static Iterable<Object[]> Test(){
        return Arrays.asList(new Object[][]{
                {"2.88",2.88},
                {"4.97",4.97},
                {"7.56",7.56},
        });
    }
    @Test
    public void stringToNumberDouble(){
        Assertions.assertEquals(expectedDouble, new ConversionFunctions().stringToNumberDouble(number));

    }

}
