import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

@RunWith(Parameterized.class)
public class ConversionFunctionsParamTestNumberDoubleToString {

    private double number;
    private String expectedString;


    public ConversionFunctionsParamTestNumberDoubleToString(double number, String expectedString ){

        this.number=number;
        this.expectedString=expectedString;
    }
    @Parameterized.Parameters()
    public static Iterable<Object[]> Test(){
        return Arrays.asList(new Object[][]{
                {2.3,"2.3"},
                {4.8,"4.8"},
                {7.9,"7.9"},
        });
    }
    @Test
    public void numberDoubleToString(){
        Assertions.assertEquals(expectedString, new ConversionFunctions().numberDoubleToString(number));

    }

}
