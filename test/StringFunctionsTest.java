
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StringFunctionsTest {

    @Test
    public void shortesWorld() {
        String str = "Hello, I am a new short word";
        int expected=1;
        int result=StringFunctions.shortesWorld(str);
        assertEquals(expected,result);
    }

    @Test
    void wordReplace() {
        String[] arrays={"Hello" , "my" , "best","friend"};
        String expected="[Hello, my, best, fri$]";
        String result=StringFunctions.wordReplace(arrays,6);
        assertEquals(expected,result);
    }

    @Test
    void addSpace() {
        String str = "Hello,I am a new short word";
        String expected="Hello, I am a new short word";
        String result=StringFunctions.addSpace(str);
        assertEquals(expected,result);
    }

    @Test
    void saveOneRepeatingCharacter() {
        String str = "Hello,world";
        String expected="He,world";
        String result=StringFunctions.saveOneRepeatingCharacter(str);
        assertEquals(expected,result);
    }

    @Test
    void countWords() {
        String str = "Hello,world";
        int expected=2;
        int result=StringFunctions.countWords(str);
        assertEquals(expected,result);
    }

    @Test
    void deletePartOfString() {
        String str = "Hello,world";
        int indexOf = 2;
        int count = 5;
        String expected="Heworld";
        String result=StringFunctions.deletePartOfString(str, indexOf,count);
        assertEquals(expected,result);
    }

    @Test
    void reverse() {
        String str = "Hello,world";
        String expected="dlrow,olleH";
        String result=StringFunctions.reverse(str);
        assertEquals(expected,result);
    }

    @Test
    void deleteLastWord() {
        String str = "Hello,world";
        String expected="Hello";
        String result=StringFunctions.deleteLastWord(str);
        assertEquals(expected,result);
    }
}