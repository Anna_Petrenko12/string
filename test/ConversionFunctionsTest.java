import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConversionFunctionsTest {

    @Test
    void numberToString() {
        int number =8;
        assertEquals("8", ConversionFunctions.numberToString(number));
    }

    @Test
    void numberDoubleToString() {
        double number =8.3;
        assertEquals("8.3", ConversionFunctions.numberDoubleToString(number));
    }

    @Test
    void stringToNumber() {
        String str ="2555";
        assertEquals(2555, ConversionFunctions.stringToNumber(str));
    }

    @Test
    void stringToNumberDouble() {
        String str ="25.36";
        assertEquals(25.36, ConversionFunctions.stringToNumberDouble(str));
    }
}