public class ConversionFunctions {
    public static void main(String[] args) {
    System.out.println("Целое число в строку "+numberToString(2));
    System.out.println("Вещественное число в строку " + numberDoubleToString(5.3));
    System.out.println("Строка в целое число "+stringToNumber("568"));
    System.out.println("Строка в вещественное число "+stringToNumberDouble("568.25"));
    }

    public static String numberToString(int number){
        return String.valueOf(number);
    }

    public static String numberDoubleToString(double number){
        return String.valueOf(number);
    }

    public static int stringToNumber(String string){
        return Integer.parseInt(string);
    }
    public static double stringToNumberDouble(String string){
        return Double.parseDouble(string);
    }
}
