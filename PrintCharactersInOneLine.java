public class PrintCharactersInOneLine {
    public static void main(String[] args) {
    englichAlphabetCapital();
    englichAlphabetSmall();
    russiaAlphabetSmall();
    number();
    tableASCII();

    }

    public static void englichAlphabetCapital(){
        System.out.println("Английский алфавит от A до Z:");
        StringBuilder bigAlphabet = new StringBuilder();
        for (char i='A';i<='Z';i++){
           System.out.print(" "+i);
        }
    }

    public static void englichAlphabetSmall(){
        System.out.println("\nАнглийский алфавит от a до z:");
        StringBuilder smallAlphabet = new StringBuilder();
        for (char i='z';i>='a';i--){
            System.out.print(" "+i);
        }
    }

    public static void russiaAlphabetSmall(){
        System.out.println("\nРусский алфавит от а до я: ");
        StringBuilder smallAlphabet = new StringBuilder();
        for (char i='а';i<='я';i++){
            System.out.print(" "+i);
        }
    }

    public static void number(){
        System.out.println("\nЦифры от 0 до 9:");
        StringBuilder number = new StringBuilder();
        for (char i='0';i<='9';i++){
            System.out.print(" "+i);
        }
    }
    public static void tableASCII(){
        System.out.println("\nТаблица ASCII: ");
        StringBuilder table = new StringBuilder();
        for (char i=33;i<256;i++){
            System.out.print(i);
        }
    }
}
