import java.util.Arrays;
public class StringFunctions {

    public static void main(String[] args) {
        StringFunctions stringFunctions = new StringFunctions();
        System.out.println(stringFunctions.shortesWorld("Hello, I am a new short word"));

        String[] arrays={"Hello" , "my" , "best","friend"};
        System.out.println(stringFunctions.wordReplace(arrays,6));
        System.out.println(stringFunctions.addSpace("Hello,world"));
        System.out.println(stringFunctions.saveOneRepeatingCharacter("Hello,world"));
        System.out.println(stringFunctions.countWords("Hello,world"));
        System.out.println(stringFunctions.deletePartOfString("Hello,world",2,5));
        System.out.println(stringFunctions.reverse("Hello,world"));
        System.out.println(stringFunctions.deleteLastWord("Hello,world"));

    }


    public static int shortesWorld(String str){
        String[] string=str.split("\\W+");
        int min=string[0].length();
        for (String world:string) {
            if (world.length() != 0) {
                if ( world.length()<min) {
                    min = world.length();
                }
            }
        }
        return min;
    }
    public static String wordReplace(String[] array, int length){
        for (int i =0; i < array.length;i++) {
            if (array[i].length()==length) {
              int threeLastWords = length - 3;
              array[i]=array[i].substring(0,threeLastWords)+"$";
            }
        }
        return Arrays.toString(array);
    }

    public static String addSpace(String string){
        return string.replace(",",", ");
    }

    public static String saveOneRepeatingCharacter(String string){
        char[] chars=string.toCharArray();
        StringBuilder sb=new StringBuilder();
        boolean reapitedChar;
        for (int i=0;i<chars.length;i++){
            reapitedChar=false;
            for (int j=i+1;j< chars.length;j++){
                if(chars[i]==chars[j]){
                    reapitedChar=true;
                    break;
                }
            }
            if (!reapitedChar){
                sb.append(chars[i]);
            }
        }
        return sb.toString();
    }

    public static int countWords(String str){
        int count = 0;
        String[] string=str.split("\\W+");
        for (String splitter : string){
            count++;
        }
        return count;
    }
    public static String deletePartOfString(String string, int indexOf, int count){
    int indexFrom=(indexOf-1)+count;
    StringBuilder sb=new StringBuilder(string);
    sb.replace(indexOf,indexFrom,"");
    return sb.toString();
    }

    public static String reverse(String string){
        StringBuffer stringBuffer=new StringBuffer(string);
        return stringBuffer.reverse().toString();
    }

    public static String deleteLastWord(String str){
        String[] array=str.split("\\W+");
        array[array.length-1]="";
        str = Arrays.toString(array);
        str=str.replace(", ","").replace("[","").replace("]","").trim();
        return str;
    }
}
